import com.devcamp.NewJavaDemo;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        //String devcamp = new String("Hello Devcamp!");
        String devcamp = "Hello Devcamp!";
        System.out.println(devcamp);

        int n = 10;
        System.out.println("n = " + n);//chu thich tren 1 dong

        /*
         * Các phương thức của lớp String
         * 
         */
        System.out.println("length của " + devcamp + " là: " + devcamp.length());
        System.out.println("upper case của " + devcamp + " là: " + devcamp.toUpperCase());
        System.out.println("lower case của " + devcamp + " là: " + devcamp.toLowerCase());


        NewJavaDemo demo = new NewJavaDemo();
        System.out.println(demo.name("Nguyen Van Nam", 25));
        demo.name(49, "Tran Van Thang");

        System.out.println(demo.name("Le Tuan"));
        demo.name("Nguyen Hien", 40, "Viet Nam");

        System.out.println(NewJavaDemo.name("Le Tuan"));
        NewJavaDemo.name("Nguyen Hien", 40, "Viet Nam");
    }
}
