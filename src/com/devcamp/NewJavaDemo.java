package com.devcamp;

import javax.crypto.spec.DESKeySpec;

public class NewJavaDemo {    

    public static void main(String[] args) {
        System.out.println("Hello Devcamp 120!");

        NewJavaDemo demo = new NewJavaDemo();
        System.out.println(demo.name("Nguyen Van Nam", 25));
        demo.name(49, "Tran Van Thang");

        System.out.println(demo.name("Le Tuan"));
        demo.name("Nguyen Hien", 40, "Viet Nam");

        System.out.println(NewJavaDemo.name("Le Tuan"));
        NewJavaDemo.name("Nguyen Hien", 40, "Viet Nam");
    }
    
    public String name(String name, int age) {
        return "My name is: " + name + ", my age is: " + age;
    }

    public void name(int age, String name) {
        System.out.println("My name is: " + name + ", my age is: " + age);
    }

    public static void name(String name, int age, String country) {
        System.out.println("My name is: " + name + ", my age is: " + age + ", my country is: " + country);
    }

    public static String name(String name) {
        return "My name is: " + name;
    }
}
